﻿///updated at 20/12/2016

///#1
///#2
///#3 16/03/2014
///#4 22/03/2014
///#5 26/03/2014
///#6 29/03/2014
///#7 20/06/2014
///#8 02/06/2015
///#9 20/12/2016
///#10 17/02/2017

//$.browser
;(function(d){var c,b;d.match=function(a){a=a.toLowerCase();a=/(chrome)[ \/]([\w.]+)/.exec(a)||/(webkit)[ \/]([\w.]+)/.exec(a)||/(opera)(?:.*version|)[ \/]([\w.]+)/.exec(a)||/(msie) ([\w.]+)/.exec(a)||0>a.indexOf("compatible")&&/(mozilla)(?:.*? rv:([\w.]+)|)/.exec(a)||[];return{browser:a[1]||"",version:a[2]||"0"}};c=d.match(navigator.userAgent);b={};c.browser&&(b[c.browser]=!0,b.version=c.version);b.chrome?b.webkit=!0:b.webkit&&(b.safari=!0);d.browser=b})(jQuery)

//Rocketman 3.0.1
;(function($) {
	var version = 'Rocketman, 3.0.1', counter = 0, storage = []

	$.fn.version = function() {return version}

	///fastest ///+1 to karma
	Element.prototype.closestByClassName = function(className) { /*IE 7 has no proptotypes*/ 
		return this.className && ~this.className.indexOf(className.toLowerCase()) /*this.className.toLowerCase() == className.toLowerCase()*/
			? this
			: this.parentNode.closestByClassName && this.parentNode.closestByClassName(className)
	}

	///fastest
	function replaceHTML (element, code) {
		var a = typeof element === 'string' ? document.getElementById(element) : element
		new insertHTML(a, code)
		a.parentNode.removeChild(a)
	}

	///fastest
	function insertHTML(element, code) {
		var bug = $.browser.opera && $.browser.version.slice(0, 2) == 12
		if (element.insertAdjacentHTML && !bug) element.insertAdjacentHTML('beforeBegin', code)
		else {
			var buffer = document.createDocumentFragment()
			var div = document.createElement('div')
			div.innerHTML = code
			var child = div.firstChild
			while (child) {
				var a = child.nextSibling
				buffer.appendChild(child)
				child = a
			}
			element.parentNode.insertBefore(buffer, element)
		}
	}

	$.fn.options = function(a) {
		return 'boolean' == typeof a ? this.find(a ? 'li.selected' : 'li.pre-selected') : this.find('ul').slice(0, 1)
	}

	///good one
	$.fn.tumbler = function(b) {
/* */ 	if (!this[0]) return false
		var a = this[0].className
		this[0].className = ~a.indexOf(b) ? a.replace(new RegExp(' ?' + b + '( (?=$))?'), '') : a && a.length ? a + ' ' + b : a + b
		return this
	}

	///*#4*/
	$.fn.refreshSelect = function(a) {
		var b = this[0], c = counter
		while (c --) {if (storage[c][0].nextSibling.id == this[0].id) break} ///object definition

		storage[c].settings.prefix = storage[c].settings.prefix.slice(0, storage[c].settings.prefix.length - 1)
		a = $.extend({}, storage[c].settings)
		storage[c].remove()
		delete storage[c][0].id

		$(b).performSelect(a)
	}

	///good one
	$.fn.displaySelect = function() {
		var b = this.tumbler('ongoing')
		var c = b[2][0] /*#5*///c[0] replaced by 'c'

		b.state = !b.state /*#5*/
		c.style.display = b.state ? '' : 'none' /*#5*/

		if (b.state) {
			b.cuselRex.outcome ++
			b.options().currentOption()

			if (b.settings.method[0]) {
				///MSIE CSS2.1
				if ($.browser.msie && c.previousSibling.nodeName.toLowerCase() == 'css3-container') document.body.appendChild(c.previousSibling)
				document.body.appendChild(c)

				///subtle positioning
				var d, e = [document.compatMode != 'CSS1Compat' || $.browser.chrome ? document.body : document.documentElement], f =  []
				///var d, e = c[0].currentStyle || window.getComputedStyle(c[0], null), f = [document.compatMode != 'CSS1Compat' ? document.documentElement : document.body]

				window.onresize = window.onscroll = function(){
					d = b.offset()
					///e = [e.backgroundPosition.split(/\s/), e.padding.split(/\s/)], then f = []

					e[1] = c.offsetHeight ///plumb
					f[1] = !1

					///if (document.documentElement.scrollHeight - d.top > b[0].offsetHeight + e[1] || d.top <= e[1]) e[1] = -b[0].offsetHeight
					if (e[0].scrollTop + document.documentElement.clientHeight - d.top > b[0].offsetHeight + e[1] || d.top <= e[1]) e[1] = -b[0].offsetHeight
					else f[1] = true

					e[2] = -b[0].offsetWidth + c.offsetWidth ///aflat
					f[2] = !1
					if (Math.max(e[0].scrollWidth, e[0].clientWidth) - d.left > c.offsetWidth) e[2] = 0
					else f[2] = true

					if (f[1] && !~c.className.indexOf('bottom-up') || !f[1] && ~c.className.indexOf('bottom-up')) b[2].tumbler('bottom-up')
					if (f[2] && !~c.className.indexOf('left-side') || !f[2] && ~c.className.indexOf('left-side')) b[2].tumbler('left-side')

					c.style.position = 'absolute'
					for (var i in d) {c.style[i] = d[i] - e[i.length % 3 + 1] + 'px'} /*#5*/

					return false
				}
				window.onresize()
			}
		}
		else b.settings.method[0] && b.append(c)

///+1 to karma
///			///non-IE
///			if (typeof window.innerWidth == 'number') f = [window.innerWidth, window.innerHeight]
///			else
///			///IE > 6 in standards compliant mode
///			if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) f = [f[0].clientWidth, f[0].clientHeight]
///			else
///			///IE 4 compatible
///			if (document.body && (document.body.clientWidth || document.body.clientHeight)) f = [f[1].clientWidth, f[1].clientHeight]

	}

	///good one
	$.fn.currentOption = function() {
		var b = this, c = b.options(false)[0] || b.options(true)[0]
		c && b[0].hasOwnProperty('scrollTo') && b[0].scrollTo(c.offsetTop - b[0].childNodes.item(1 == b[0].childNodes.item(0).nodeType ? 0 : 1).offsetTop) /*#6*/ /*#8*/
	}

	$.fn.performSelect = function(a) {
		a = $.extend({}, a)

		///defaults
		a.arrows = (typeof a.arrows == 'undefined' || a.arrows === true) ? true : false /*#4*/
		a.canvas = a.canvas || 'outside shadow or borders'
///		a.memory = [] ///inward setting, replaced by storage
		a.output = a.output || 12
		a.prefix = a.prefix && a.prefix + '-' || ''

		///a.method = [boolean, boolean] <- 'common basis', 'initial select retention'
		///[''] || [false] = [false, false]
		///[false, true]
		///[true] = [true, false] by default

		if (typeof a.method == 'undefined') a.method = [] //window.a.method
		a.method = !a.method || a.method.length ? ['boolean' == typeof a.method[0] && a.method[0], 'boolean' == typeof a.method[1] && a.method[1] || false] : [true, false]

		this.each(function() {
			var b = this
			if (b.multiple) return false ///do not supported

			///user id permission
			b.id && counter ++

			///classes
			var c = [b.disabled && 'disabled', a.arrows && 'arrowed', b.className, a.prefix + 'select'], d = 4
			while (d --) {if (c[d] == !1 || '') {c.splice(d, 1)}}

///			var e, f = b.innerHTML.replace(/option/gi, 'li')
			var e, f = b.innerHTML /*#3*/
			f = /option/.test(f.toLowerCase()) ? f.replace(/option/gi, 'li') : '<li></li>' /*#3*/

			///optgroups
			if (~f.search(/optgroup/)) {
				function label(a) {return ~a.search(/label/) ? '<li class="optgroup"><span>' + d[e ++].getAttribute('label') + '</span><ul>' : '</ul>'}
				d = b.getElementsByTagName('optgroup')
				e = 0
				f = f.replace(/<\/?optgroup[^>]*>/gi, label)
			}/* */

			d = b.id || 'select-' + counter ++ ///common selects counter
///			e = (e || 0) + b.selectedIndex
			e = (e || 0) + Math.max(b.selectedIndex, 0) /*#3*/

			f = '' +
			'<div class="' + c.join('&#32;') + '" ' + 'id="custom-' + d + '" ' + 'tabindex="' + b.tabIndex + '">' +
				'<div class="pointer"></div>' +
				'<div class="defined value">' + (b.options[0] ? b.options[b.selectedIndex].text : '') + '</div>' + /*#4*/

				///class="outside shadow or borders (for custom-select-n named somehow)*" (id="classed somehow or select")*
				///* — '<div class="' + a.canvas + '">' or '<div class="' + a.canvas + ' for custom-' + d + ' named ' + b.name +'" id="classed somehow or select">'

///				'<div class="' + a.canvas + (a.method[0] ? ' for custom-' + d + (b.name ? ' named ' + b.name : '') + '" id="' + (b.className ? b.className : a.prefix + 'select') : '') + '">' +
				'<div class="' + a.canvas + (a.method[0] ? ' for custom-' + d + '" id="' + (b.className ? b.className : a.prefix + 'select') : '') + '">' +
					'<div class="top eraser"></div>' +
					'<ul>' + f.replace(/undefined/gi, '') + '</ul>' +
					'<div class="bottom eraser"></div>' +
				'</div>' +
			'</div>' ///-:c

			a.method[1] ? new insertHTML(b, f) : new replaceHTML(b, f + '<input id="' + d + '" type="hidden" name="' + b.name + '" value="' + b.value + '" />')
			if (a.method[1] && !b.id) b.id = d

			if (a.method[1]) b.style.display = 'none' ///a.method[1] && (b.style.display = 'none') ///(): IE < 9
			else document.getElementById(d).onchange = b.onchange

			c = document.getElementById('custom-' + d)
			d = c.getElementsByTagName('li') ///+:b c d e -:f

			d[e].className = 'selected'
			d[e].removeAttribute('selected')

			e = d[e]

			f = e.currentStyle || window.getComputedStyle(e, null)
			f = [parseInt(f['marginTop'], 10), parseInt(f['marginBottom'], 10)]

			//if (d.length > a.output) c.getElementsByTagName('ul')[0].style.height = (e.offsetHeight + Math.max(f[0], f[1])) * a.output + Math.min(f[0], f[1]) + 'px'
			// костыль
			if (d.length > a.output) c.getElementsByTagName('ul')[0].style.height = 252 + 'px'

			///+:b c -:d e f
			storage.push(new $(c)) /*newborn object*/

			///united depot
			b = storage[counter - 1]
/* */		delete b.context
/* */		b[1] = b.children().slice(-2)[0]
/* */		b[2] = b.children().slice(-1)

///			storage[counter - 1].a = a /*#1*/
			storage[counter - 1].state = false /*#5*/
			storage[counter - 1].settings = a /*#5*/

			b.cuselRex = {version: version, outcome: 0} ///this library version and outcome times

			///default & ratable widths
			b.options().jScrollPaneCusel_({jScrollArrows: a.arrows, jScrollPrefix: a.prefix})
			///subtle width
			/*IE7 problem*///c.style.width = Math.max(parseInt((c.currentStyle || window.getComputedStyle(c, null)).width, 10), c.lastChild.offsetWidth) + 'px'

		//	/*#10*/ c.style.width = Math.max(c.offsetWidth, c.lastChild.offsetWidth) + 'px'
			/*#10*/ c.style.width = Math.max(c.clientWidth, d[0].parentNode.clientWidth) + 'px'
			/*#10*/ c.lastChild.style.display = 'none'

			///function just(a) {return a.currentStyle || window.getComputedStyle(a, null)}

			b.selectsCallback(e.value, true)

			b.on('mouseenter mouseleave', function() {b.tumbler('focused')})
			b.settings.method[0] && b[2].on('mouseenter mouseleave', function() {b.tumbler('focused')}) /*#3*/

			b.on('keydown', function(event) {
				///this == b[0]
				if (~this.className.indexOf('disabled')) return false

				var c = window.event ? window.event.keyCode : event && event.which, d = [b[2].options(false), b[2].options(true)]
				if (c == null || c == 0 || c == 9) return true ///NULL&TAB

				function down(c) {
					b.state = true
					b.settings.method[0] && b.mouseenter()
					b.save(c || d[0]) /*#6*/
				}

				///selection and ENTER
				if (c == 13) {
					d[0][0] ? new down() : $.browser.chrome && b.displaySelect() ///standart behaviour
					return false
				}

				///ESC
				if (c == 27) new hide()

				///keystrokes (38 or 40)
				var e = new around(d[0][0] ? d[0] : d[1], c == 40 ? 'next' : c == 38 ? 'previous'.slice(0, 4) : false) ///optgroups /*#5*/
				if (e.length) {
					if (d[1][0] != d[0][0]) d[0].tumbler('pre-selected')
					if (d[1][0] != e[0]) e.tumbler('pre-selected')

					b[1].innerHTML = e[0].innerHTML

					d[2] = b[2].options()[0]
					d[2].hasOwnProperty('scrollTo') && d[2].scrollTo(e.position().top) /*#8*/

					///standart behaviour
					!$.browser.mozilla && !b.state && new down(e)

					return false
				}

				if (c == 32) {
					!b.state && $.browser.chrome && b.displaySelect()
					$.browser.opera && b.displaySelect()

					return false
				}

				function around(a, b) { /*#5*/
					var c = b == 'next' ? 0 : -1
					return 	b
							?	a[b]().options()[0]
								?	a[b]().options().find('li').slice(c).first()
								:	a[b]()[0]
									?	a = a[b]()
									:	a.parents('li')[b]().options()[0]
										? a.parents('li')[b]().options().find('li').slice(c).first()
										: a = a.parents('li')[b]()
					 		: false

///					Q1 ? E1Y : (Q2 ? E2Y : (Q3 ? E3Y : E3N))

///					var d = a[b]()
///					var e = function(a) {return a.options().find('li').slice(b == 'next' ? 0 : -1).first()}
///					var f

///					if (d.options()[0]) /*Q1*/ f = new e(d) /*E1Y*/
///					else {
///						if (d[0]) /*Q2*/ f = d /*E2Y*/
///						else {
///							d = a.parents('li')[b]()
///							if (d.options()[0]) /*Q3*/ f = new e(d) /*E3Y*/
///							else f = d /*E3N*/
///						}
///					}
///					return f
				}
			})

			var aZ = []
			b.on('keypress', function(event) {
/* => */		///this == b[0]
				if (~this.className.indexOf('disabled')) return false

				var c = window.event ? window.event.keyCode : event && event.which
				if (c == null || c == 0 || c == 9) return true ///NULL&TAB
/* <= */

				if (c == 13) return false

				aZ.push(c)

				if (typeof timer != 'undefined') clearTimeout(timer)
				timer = setTimeout(function() {new find()}, 400)
				
				function find() {
					var c = []
					for (var d in aZ) {c[d] = String.fromCharCode(aZ[d]).toLowerCase()}

					d = b[2][0].getElementsByTagName('li')

					for (var e in d) {
						if (+e >= 0) { /*#5*///d['length']
///							var f = d.item(e), g = null
							var f = d[e], g = null /*#5*/

							for (var h in aZ) {if (f.innerHTML.charAt(h).toLowerCase() != c[h]) g = true}
							if (g) continue

							b[1].innerHTML = f.innerHTML
							b[2].options(false).tumbler('pre-selected')

							new $(f).tumbler('pre-selected')
							b[2].options().currentOption()
						}
						aZ = [] ///cleaning
						return true
					}
					aZ = [] ///no matches reset
				}
			})
		})

///+1 to karma
///		var array = []
///		console.log(Math.max.apply(0, array))

/* => */ /*#4*/ /*#5*/
		document.onclick = function(b) {
			var b = b ? b.target : window.event.srcElement, c = counter, d = b.parentNode

///			///object[html] definition ///+1 to karma
///			var b = counter, c
///			while (b --) {
///				c = storage[b][0]
///				if ((c.compareDocumentPosition ? c.compareDocumentPosition(this.target) : c.contains(this.target)) & 20) break
///			}

///			///nesting definition ///+1 to karma
///			var b = ... /*inital node*/, c = 0
///			while (b && b.property != value) {
///				b = b.parentNode
///				c ++
///				if (b == document) c = -2 ///return b = -1
///			}

///			if (b.closestByClassName('disabled')) new hide() /*#7*/
///			else /*#7*/
			if (d != document) {
				while (c --) {
					///click on 'select' classed division
					if (b.id == storage[c][0].id && !~b.className.indexOf('disabled') /*#7*/) {
						new show(storage[c])
						///console.log('div classed "select"')
					}
					else
					///click on 'defined value' or 'pointer' classed divisions
					if (d.id == storage[c][0].id && !~d.className.indexOf('disabled') /*#7*/) {
						new show(storage[c])
						///console.log('div classed "defined value" or "pointer"')
					}
					else
///					if (~storage[c][0].className.indexOf('ongoing')) {
					if (storage[c].state) { /*#5*/
						if (b.closestByClassName(storage[c].settings.canvas)) {
							///click on selected item
							if (b.nodeName.toLowerCase() == 'li' && !~b.className.indexOf('selected')) {
								///console.log('selected item')
								storage[c].save(new $(b)) /*#6*/
							}
						}
						else new hide()
					}
				}
			}
			///return false /*addEventListener method needs 'b.stopPropagation ? b.stopPropagation() : (b.cancelBubble = true)'*/
		}
/* <= */ /*#4*/

		function hide(b) {
			var c = counter ///cross-method variant:
///			while (c --) {if (~storage[c][0].className.indexOf('ongoing')) break} ///owner definition
			while (c --) {if (storage[c].state) break} ///owner definition /*#5*/

			if (!~c) return false ///no matches
			c = storage[c]

///			if (!!b) return c ///once used /*#6*/

			c[1].innerHTML = c[2].options(true)[0].innerHTML
			c[2].options(false).tumbler('pre-selected')

			c.displaySelect() /*#5*/
		}

		function show(b) {
			new hide()

///			var c = counter
///			while (c --) {if (b.id == storage[c][0].id) storage[c].displaySelect(storage[c].a)} ///object definition /*#1*/

			b.displaySelect() /*#5*/
		}

		$.fn.save = function(a) { /*#6*/
			var b = this

			b[1].innerHTML = a[0].innerHTML
			b[2].options(true).tumbler('selected')
			b[2].options(false).tumbler('pre-selected')

			a[0].className = 'selected'

			var c = b[0].nextSibling

			b.settings.method[0] && b.mouseleave() /*#3*/ /*#4*/ /*#5*/
			b.settings.method[1] ? c.options[a.index()].selected = true : c.value = $(a[0]).attr('value');/*#3*/ /*#5*/

			b.displaySelect() /*#5*/
			b.selectsCallback(c.value, false)

			c.onchange && c.onchange()

			///for DOM only
			if (b.settings.method[1]) { /*#4*/
				c.options[c.selectedIndex].removeAttribute('selected')
				c.options[a.index()].setAttribute('selected', 'selected')
			}
		}
	}
})(jQuery)


//var ua = navigator.userAgent.toLowerCase()
//var isOpera = (ua.indexOf('opera')  > -1)
//var isIE = (!isOpera && ua.indexOf('msie') > -1)

//function getDocumentHeight() {
//	return Math.max(document.compatMode != 'CSS1Compat' ? document.body.scrollHeight : document.documentElement.scrollHeight, getViewportHeight())
//}

//function getViewportHeight() {
//	return ((document.compatMode || isIE) && !isOpera) ? (document.compatMode == 'CSS1Compat') ? document.documentElement.clientHeight : document.body.clientHeight : (document.parentWindow || document.defaultView).innerHeight
//}


;(function(a){a.fn.selectsCallback=function(a,b){this.value=a;"function"==typeof selectsCallback&&new selectsCallback(this,b)}})(jQuery)
;(function(a){function d(b){var c=b||window.event,d=[].slice.call(arguments,1),e=0,f=!0,g=0,h=0;return b=a.event.fix(c),b.type="mousewheel",c.wheelDelta&&(e=c.wheelDelta/120),c.detail&&(e=-c.detail/3),h=e,c.axis!==undefined&&c.axis===c.HORIZONTAL_AXIS&&(h=0,g=-1*e),c.wheelDeltaY!==undefined&&(h=c.wheelDeltaY/120),c.wheelDeltaX!==undefined&&(g=-1*c.wheelDeltaX/120),d.unshift(b,e,g,h),(a.event.dispatch||a.event.handle).apply(this,d)}var b=["DOMMouseScroll","mousewheel"];if(a.event.fixHooks)for(var c=b.length;c;)a.event.fixHooks[b[--c]]=a.event.mouseHooks;a.event.special.mousewheel={setup:function(){if(this.addEventListener)for(var a=b.length;a;)this.addEventListener(b[--a],d,!1);else this.onmousewheel=d},teardown:function(){if(this.removeEventListener)for(var a=b.length;a;)this.removeEventListener(b[--a],d,!1);else this.onmousewheel=null}},a.fn.extend({mousewheel:function(a){return a?this.bind("mousewheel",a):this.trigger("mousewheel")},unmousewheel:function(a){return this.unbind("mousewheel",a)}})})(jQuery)

// ==ClosureCompiler==
// @compilation_level SIMPLE_OPTIMIZATIONS
// ==/ClosureCompiler==

;(function($) {
	$.jScrollPaneCusel_ = {active: []}
	$.fn.jScrollPaneCusel_ = function(settings) {
		settings = $.extend({}, $.fn.jScrollPaneCusel_.defaults, settings)
		var rf = function() {return false}

		//$.browser.chrome = ($.browser.safari && /chrome/.test(navigator.userAgent.toLowerCase())) ? true: false

		//console.log(this)

		return this.each(function() {

			//var paneWidth = this.parentNode.offsetWidth, $this = $(this).css('overflow', 'hidden')
		//	/*#10*/ var paneWidth = this/*.parentNode*/.offsetWidth, $this = $(this).css('overflow', 'hidden')
			/*#10*/ var paneWidth = this.parentNode.parentNode.clientWidth, $this = $(this).css('overflow', 'hidden')

			var paneEle = this
			if ($(this).parent().is('.' + settings.jScrollPrefix + 'container')) {
/*#10			var currentScrollPosition = $this.position().top
				var $c = $(this).parent()
				var paneHeight = $c.outerHeight()
				var trackHeight = paneHeight
				$('>.scroll, >.up.arrow, >.down.arrow', $c).remove()
				$this.css('top', 0)
*/
//				console.log(1)
			}
			else {
				var currentScrollPosition = 0
				this.originalPadding = $this.css('paddingTop') + ' ' + $this.css('paddingRight') + ' ' + $this.css('paddingBottom') + ' ' + $this.css('paddingLeft')
				var paneHeight = $this.innerHeight()

				//var paneHeight = $this.outerHeight(true)
				var trackHeight = paneHeight
				$this.wrap('<div class="' + settings.jScrollPrefix + 'container"></div>').parent().css({height: paneHeight + 'px', width: paneWidth /*- $this.css('paddingRight') - $this.css('paddingLeft')*/ + 'px'})
				$(document).bind('emchange', function(e, cur, prev) {$this.jScrollPaneCusel_(settings)})
//				console.log(2)
			}

			var p = this.originalSidePaddingTotal //??

			$this.css({height: 'auto', position: 'absolute'})

			var contentHeight = $this.outerHeight()
			var percentInView = paneHeight / contentHeight

			if (percentInView < .99) {
				var $container = $this.parent()

				var containerPaddingTop = parseInt($container.css('paddingTop'), 10) || 0
				var containerPaddingBottom = parseInt($container.css('paddingBottom'), 10) || 0
				var containerPadding = containerPaddingTop + containerPaddingBottom

				$container.append(
					$('<div class="scroll"></div>').append(
						$('<div class="slider"></div>').append(
							$('<div class="top tip"></div>'), $('<div class="bottom tip"></div>')
						)
					)
				)

				var $track = $('>.scroll', $container)
				var $drag = $('>.scroll .slider', $container)

				var tipsHeight = $('>.tip', $drag).eq(0).height() + $('>.tip', $drag).eq(1).height()

				if (settings.jScrollArrows) {
					var currentArrowButton
					var currentArrowDirection
					var currentArrowInterval
					var currentArrowInc
					var whileArrowButtonDown = function() {
						if (currentArrowInc > 4 || currentArrowInc % 4 == 0) {
//							performAnimation(-currentArrowDirection)
							positionDrag(dragPosition + currentArrowDirection * mouseWheelMultiplier)
						}
						currentArrowInc ++
					}
					var onArrowMouseUp = function() {//event) {
						$('html').unbind('mouseup', onArrowMouseUp)
						currentArrowButton.removeClass('affected')
						clearInterval(currentArrowInterval)
					}
					var onArrowMouseDown = function() {
						$('html').bind('mouseup', onArrowMouseUp)
						currentArrowButton.addClass('affected')
						currentArrowInc = 0
						whileArrowButtonDown()
						currentArrowInterval = setInterval(whileArrowButtonDown, 100)
					}

					$container.append(
						$('<div></div>')
							.addClass('up arrow')
							.bind('mousedown', function() {
								currentArrowButton = $(this)
								currentArrowDirection = -1
								onArrowMouseDown()
								this.blur()
								return false
							}).bind('click', rf),
						$('<div></div>')
							.addClass('down arrow')
							.bind('mousedown', function() {
								currentArrowButton = $(this)
								currentArrowDirection = 1
								onArrowMouseDown()
								this.blur()
								return false
							}).bind('click', rf)
					)
					var $upArrow = $('>.up.arrow', $container)
					var $downArrow = $('>.down.arrow', $container)

					var upArrowHeight = $upArrow.height() || 0
					var downArrowHeight = $downArrow.height() || 0

					trackHeight = paneHeight - upArrowHeight - downArrowHeight - containerPadding
					$track.css({height: trackHeight + 'px', top: upArrowHeight + containerPadding + 'px'})
				}
				else {
					var upArrowHeight = downArrowHeight = 0
				}

				var $pane = $this
				
				var currentOffset
				var maxY
				var mouseWheelMultiplier
				var dragPosition = 0
				var dragMiddle = percentInView * paneHeight / 2
				
				var getPos = function (event, c) {
					var p = c == 'X' ? 'Left' : 'Top'
					return event['page' + c] || (event['client' + c] + (document.documentElement['scroll' + p] || document.body['scroll' + p])) || 0
				}

				var ignoreNativeDrag = function() {return false}

				var elementHeight = $this.children('li.selected').outerHeight()
				var elementOffset = Math.max(parseInt($this.children('li.selected').css('marginTop'), 10), parseInt($this.children('li.selected').css('marginBottom'), 10)) || 0

				var initDrag = function() {
/*0.4*///			ceaseAnimation()
		//			currentOffset = $drag.offset(false) BUG jquery 1.8.X
					currentOffset = $drag.offset() //BUG jquery 1.8.X
					currentOffset.top -= dragPosition
					maxY = trackHeight - $drag[0].offsetHeight
/*0.4*/				mouseWheelMultiplier = 3 * maxY * (elementHeight + elementOffset) / (contentHeight - paneHeight)
					if ($.browser.msie) mouseWheelMultiplier = 2 * mouseWheelMultiplier
/*0.4*/				if ($.browser.chrome) mouseWheelMultiplier = parseInt(100 * maxY / (contentHeight - paneHeight), 10) //FUCK
				}

				var onStartDrag = function(event) {
					initDrag()
					dragMiddle = getPos(event, 'Y') - dragPosition - currentOffset.top
					$('html').bind('mouseup', onStopDrag).bind('mousemove', updateScroll)
					if ($.browser.msie) {$('html').bind('dragstart', ignoreNativeDrag).bind('selectstart', ignoreNativeDrag)}
					return false
				}
				var onStopDrag = function() {
					$('html').unbind('mouseup', onStopDrag).unbind('mousemove', updateScroll)
					dragMiddle = percentInView * paneHeight / 2
					if ($.browser.msie) {$('html').unbind('dragstart', ignoreNativeDrag).unbind('selectstart', ignoreNativeDrag)}
				}
				var positionDrag = function(destY) {
					destY = destY < 0 ? 0 : (destY > maxY ? maxY : destY)
					if (destY == 0 || destY == maxY) {
						ceaseAnimation()
					}
					dragPosition = destY
					$drag.css('top', destY + 'px')
					var p = destY / maxY
					$pane.css('top', parseInt(((paneHeight - contentHeight) * p).toFixed(0), 10) + (containerPaddingTop ? containerPaddingTop : 0) + 'px')
					$this.trigger('scroll')
					if (settings.jScrollArrows) {
						$upArrow[destY == 0 ? 'addClass' : 'removeClass']('disabled')
						$downArrow[destY == maxY ? 'addClass' : 'removeClass']('disabled')
					}
				}
				var updateScroll = function(e) {positionDrag(getPos(e, 'Y') - currentOffset.top - dragMiddle)}

				var dragMaxHeight = paneHeight - containerPadding - upArrowHeight - downArrowHeight
				var dragMinHeight = 15 + tipsHeight

				var dragH = Math.max(Math.min(percentInView * dragMaxHeight, dragMaxHeight), dragMinHeight)

				$drag.css('height', dragH + 'px').bind('mousedown', onStartDrag)

				var trackScrollInterval
				var trackScrollInc
				var trackScrollMousePos
				var doTrackScroll = function() {
					if (trackScrollInc > 8 || trackScrollInc % 4 == 0) {positionDrag((dragPosition - ((dragPosition - trackScrollMousePos) / 2)))}
					trackScrollInc ++
				}
				var onStopTrackClick = function() {
					clearInterval(trackScrollInterval)
					$('html').unbind('mouseup', onStopTrackClick).unbind('mousemove', onTrackMouseMove)
				}
				var onTrackMouseMove = function(event) {trackScrollMousePos = getPos(event, 'Y') - currentOffset.top - dragMiddle}
				var onTrackClick = function(event) {
					initDrag()
					onTrackMouseMove(event)
					trackScrollInc = 0
					$('html').bind('mouseup', onStopTrackClick).bind('mousemove', onTrackMouseMove)
					trackScrollInterval = setInterval(doTrackScroll, 100)
					doTrackScroll()
				}

				$track.bind('mousedown', onTrackClick)

				$container.bind('mousewheel', function (event, delta) {
					performAnimation(delta)
					return false
				})

				var _animateToPosition
				var _animateToInterval
/*0.4*/
				var _animateToMin = (trackHeight - $drag[0].offsetHeight) / (contentHeight - paneHeight)
				var _animateBoolean = true

				function animateToPosition() {
					_animateBoolean = false
					var diff = _animateToPosition - dragPosition
					//alert(diff)
					if (Math.abs(diff) > _animateToMin && diff != 0) {
						positionDrag(dragPosition + diff / 2)
					}
					else {
						positionDrag(_animateToPosition)
						ceaseAnimation()
					}
				}

				var ceaseAnimation = function() {
					if (_animateToInterval) {
						clearInterval(_animateToInterval)
						delete _animateToPosition
						_animateBoolean = true
					}
				}

				function performAnimation(dir) {
					if ($.browser.opera || $.browser.mozilla) {
						if (!_animateBoolean) {
							positionDrag(_animateToPosition)
						}
						else {
							ceaseAnimation()
							_animateToPosition = dragPosition - dir * mouseWheelMultiplier;
							_animateToInterval = setInterval(animateToPosition, 10)
						}
					}
					else {
						positionDrag(dragPosition - dir * mouseWheelMultiplier)
					}
				}
/*end*/
				var scrollTo = function(pos) {//, preventAni) {
					if (typeof pos == 'string') {
						$e = $(pos, $this)
						if (!$e.length) return
						pos = $e.offset().top - $this.offset().top
					}
					$container.scrollTop(0)

/*0.4*///			ceaseAnimation()
					var destDragPosition = -pos / (paneHeight - contentHeight) * maxY
/*0.4*/				positionDrag(destDragPosition)
				}
				$this[0].scrollTo = scrollTo
				$this[0].scrollBy = function(delta) {
					var currentPos = -parseInt($pane.css('top')) || 0
					scrollTo(currentPos + delta)
				}

				initDrag()
				scrollTo(-currentScrollPosition)//, true)

/*				$('*', this).bind('focus', function(event) {
					var $e = $(this)
					var eleTop = 0

					while ($e[0] != $this[0]) {
						eleTop += $e.position().top
						$e = $e.offsetParent()
					}

					var viewportTop = -parseInt($pane.css('top'), 10) || 0
					var maxVisibleEleTop = viewportTop + paneHeight
					var eleInView = eleTop > viewportTop && eleTop < maxVisibleEleTop
					if (!eleInView) {
						var destPos = eleTop// - settings.scrollbarMargin
						if (eleTop > viewportTop) {destPos += $(this).height() + 15 - paneHeight}
						scrollTo(destPos)
					}
				})
*/
				if (location.hash) {scrollTo(location.hash)}
				
				$(document).bind('click', function(e) {
					$target = $(e.target)
					if ($target.is('a')) {
						var h = $target.attr('href')
						if (h.substr(0, 1) == '#') {scrollTo(h)}
					}
				})

				$.jScrollPaneCusel_.active.push($this[0])
			} else {
				$this.css({
					height: paneHeight + 'px',
					width: paneWidth - this.originalSidePaddingTotal + 'px',
					padding: this.originalPadding
				})
				$this.parent().unbind('mousewheel')
			}

		$this.jScrollPaneCallback_()
		})
	}
	$.fn.jScrollPaneRemoveCusel_ = function() {
		$(this).each(function() {
			$this = $(this)
			var $c = $this.parent()
			if ($c.is('.' + settings.jScrollPrefix + 'container')) {
				$this.removeAttr('style').attr('style', $this.data('originalStyleTag'))
				$c.after($this).remove()
			}
		})
	}

	$.fn.jScrollPaneCallback_ = function() {
		if (typeof(jScrollPaneCallback) == 'function') {
			jScrollPaneCallback(this)
		}
	}

	$.fn.jScrollPaneCusel_.defaults = {jScrollArrows: false, jScrollPrefix: ''}

	$(window).bind('unload', function() {
		var els = $.jScrollPaneCusel_.active
		for (var i = 0; i < els.length; i ++) {els[i].scrollTo = els[i].scrollBy = null}
	})
})(jQuery)